import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.scss']
})
export class ConnexionComponent implements OnInit {


  ConnexionForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, private route: Router) {
    this.ConnexionForm = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
   });
   }

  ngOnInit(): void {
  }


  onConnexionBuild(){
    console.log('connexion', this.ConnexionForm);
    this.route.navigate(['information']);

  }
}
