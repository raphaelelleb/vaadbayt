import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss']
})
export class InscriptionComponent implements OnInit {

  newBuildForm!: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.newBuildForm = new FormGroup({
      contry: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required),
      street: new FormControl('', Validators.required),
      building: new FormControl('', Validators.required),
      entryNumber: new FormControl('', Validators.required),
      email: new FormControl('',[Validators.email, Validators.required]),
      cp: new FormControl(''),
      numberOfLives: new FormControl(''),
      responsable: new FormControl(''),
   });
   }

  ngOnInit(): void {
  }


  onSaveBuild(){
    console.log('immeuble enrengister', this.newBuildForm);

  }

}
